import { AbstractView } from "./views";

type Path = `/${string}`;
interface Route {
  view: new () => AbstractView;
}
type Routes = Record<Path, Route>;

export class Router {
  app: HTMLElement;
  routes: Routes;

  constructor(app: HTMLElement, routes: Routes) {
    this.app = app;
    this.routes = routes;
  }

  setup() {
    document.addEventListener("DOMContentLoaded", () => {
      document.addEventListener("click", (e) => {
        if (
          e.target instanceof HTMLAnchorElement &&
          e.target.matches("[data-link]")
        ) {
          e.preventDefault();
          this.render(e.target.pathname as Path);
        }
      });
      this.render(window.location.pathname as Path);
    });

    window.addEventListener("popstate", () => {
      this.render(window.location.pathname as Path);
    });
  }

  render(path: Path) {
    const route = this.routes[path];

    if (route) {
      history.pushState(path, "", path);
      const view = new route.view();
      view.setTitle();
      view.render(this.app);
    } else {
      this.render("/");
    }
  }
  redirect(path: Path) {
    this.render(path);
  }
}
