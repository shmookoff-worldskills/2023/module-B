import authStore from "@/stores/auth";
import { router } from "@/app";

export const initAuth = () => {
  const token = authStore.token;
  if (token === null) {
    router.redirect("/login");
  }
};
