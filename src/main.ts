import { router } from "./app";
import { initAuth } from "./auth";

router.setup();
initAuth();
