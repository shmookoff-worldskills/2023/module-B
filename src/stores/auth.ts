class AuthStore {
  private _token: string | null = window.localStorage.getItem("token");

  get token() {
    return this._token;
  }
  set token(newToken) {
    if (newToken === null) {
      window.localStorage.removeItem("token");
    } else {
      window.localStorage.setItem("token", newToken);
    }
    this._token = newToken;
  }
}

export default new AuthStore();
