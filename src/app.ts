import { Router } from "./router";
import {
  HomeView,
  LoginView,
  OrdersView,
  EmployeesView,
  ShiftsView,
  NewEmployeeView,
} from "./views";

import "./assets/css/main.css";

const app = document.querySelector<HTMLDivElement>("#app")!;

const router = new Router(app, {
  "/": { view: HomeView },
  "/login": { view: LoginView },
  "/employees": { view: EmployeesView },
  "/employees/new": { view: NewEmployeeView },
  "/shifts": { view: ShiftsView },
  "/orders": { view: OrdersView },
});

export { router };
