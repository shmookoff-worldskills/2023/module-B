import { html, TemplateResult } from "lit-html";
import { AbstractLayout } from "./abstract";

import logo from "@/assets/img/logo.png";
import authStore from "@/stores/auth";
import { router } from "@/app";

export class DefaultLayout extends AbstractLayout {
  logout() {
    authStore.token = null;
    router.redirect("/login");
  }

  async getHtml(slot: TemplateResult): Promise<TemplateResult> {
    return html`
      <header>
        <article>
          <img src="${logo}" alt="logo" />
        </article>
        <nav>
          <a href="/employees" data-link>Сотрудники</a>
          <a href="/shifts" data-link>Смены</a>
          <a href="/orders" data-link>Заказы</a>
          <a href="/login" data-link class="approve_button">Вход</a>
          <a @click=${this.logout} class="cancel_button">Выход</a>
        </nav>
      </header>
      <main>${slot}</main>
      <footer>
        <article>&copy; Tortotoro - все права защищены</article>
        <article>8(888)777-44-11</article>
      </footer>
    `;
  }
}
