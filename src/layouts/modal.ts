import { html, TemplateResult } from "lit-html";
import { AbstractLayout } from "./abstract";

export class ModalLayout extends AbstractLayout {
  async getHtml(slot: TemplateResult): Promise<TemplateResult> {
    return html`<div class="modal">${slot}</div>`;
  }
}
