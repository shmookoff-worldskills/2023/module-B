import { TemplateResult } from "lit-html";

export abstract class AbstractLayout {
  abstract getHtml(...args: any[]): Promise<TemplateResult>;
}
