import { TemplateResult, render } from "lit-html";

export abstract class AbstractView {
  abstract title: string;

  setTitle() {
    document.title = this.title;
  }

  abstract getHtml(): Promise<TemplateResult>;

  async render(element: HTMLElement): Promise<void> {
    render(await this.getHtml(), element);
  }
}
