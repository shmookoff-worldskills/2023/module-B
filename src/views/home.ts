import { AbstractView } from "./abstract";
import { html, TemplateResult } from "lit-html";
import { DefaultLayout } from "@/layouts/default";

export class HomeView extends AbstractView {
  public title = "Главная";

  async getHtml(): Promise<TemplateResult> {
    return new DefaultLayout().getHtml(html``);
  }
}
