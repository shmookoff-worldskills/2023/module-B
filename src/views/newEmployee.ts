import { ApiError } from "@/api/fetchApi";
import { getUsers, postUser } from "@/api/user";
import { router } from "@/app";
import { ModalLayout } from "@/layouts/modal";
import { html, TemplateResult } from "lit-html";
import { AbstractView } from "./abstract";

export class NewEmployeeView extends AbstractView {
  title = "Добавление сотрудника";

  async submit(e: SubmitEvent) {
    e.preventDefault();

    const form = document.getElementById(
      "new-employee-form"
    ) as HTMLFormElement;

    await postUser(form);

    router.redirect("/employees");
  }

  cancel(e: MouseEvent) {
    e.preventDefault();
    router.redirect("/employees");
  }

  async checkPermission(): Promise<boolean> {
    try {
      await getUsers();
      return true;
    } catch (err) {
      if (err instanceof ApiError && err.data.error.code === 403) {
        return false;
      }
      throw err;
    }
  }

  async getHtml(): Promise<TemplateResult> {
    if (!(await this.checkPermission())) {
      router.redirect("/employees");
      return html``;
    }
    return new ModalLayout().getHtml(
      html`<form id="new-employee-form" @submit=${this.submit}>
        <h2>Добавление нового сотрудника</h2>
        <div>
          <label for="name">Имя</label>
          <input type="text" name="name" id="name" required />
        </div>
        <div>
          <label for="login">Логин</label>
          <input type="text" name="login" id="login" required />
        </div>
        <div>
          <label for="password">Пароль</label>
          <input type="password" name="password" id="password" required />
        </div>
        <div>
          <label for="photo_file" class="photo_input">Фото</label>
          <input type="file" name="photo" id="photo_file" />
        </div>
        <div>
          <label for="role_id">Роль</label>
          <select name="role_id" id="role_id" required>
            <option value="" selected disabled>Выберите роль:</option>
            <option value="0">Администратор</option>
            <option value="1">Официант</option>
            <option value="2">Повар</option>
          </select>
        </div>
        <div>
          <button type="submit" class="approve_button">Отправить</button>
          <button @click=${this.cancel} class="cancel_button">Отмена</button>
        </div>
      </form>`
    );
  }
}
