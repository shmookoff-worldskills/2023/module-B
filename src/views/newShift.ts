import { ModalLayout } from "@/layouts/modal";
import { html, TemplateResult } from "lit-html";
import { AbstractView } from "./abstract";

export class NewShiftView extends AbstractView {
  title = "Добавить смену";

  async getHtml(): Promise<TemplateResult> {
    return new ModalLayout().getHtml(html``);
  }
}
