import { html, TemplateResult } from "lit-html";
import { until } from "lit-html/directives/until.js";
import { map } from "lit-html/directives/map.js";

import { DefaultLayout } from "@/layouts/default";
import { AbstractView } from "./abstract";
import { getUsers } from "@/api/user";
import { EmployeeComponent } from "@/components/Employee";
import { router } from "@/app";
import { ApiError } from "@/api/fetchApi";

export class EmployeesView extends AbstractView {
  public title = "Сотрудники";

  async getHtml(): Promise<TemplateResult> {
    return new DefaultLayout().getHtml(
      html`<section class="employees">
        ${until(
          getUsers()
            .then(
              (users) =>
                html`<article>
                    <span>Имя</span>
                    <span>Статус</span>
                    <span>Должность</span>
                    <button @click=${() => router.redirect("/employees/new")}>
                      +
                    </button>
                  </article>
                  ${map(users, (user) => new EmployeeComponent(user).render())}`
            )
            .catch((e) =>
              e instanceof ApiError && e.data.error.code === 403
                ? "No permission"
                : "An error has occured"
            ),
          "Loading..."
        )}
      </section>`
    );
  }
}
