import { html, TemplateResult } from "lit-html";
import { router } from "@/app";
import { loginUser } from "@/api/auth";
import { ModalLayout } from "@/layouts/modal";
import { AbstractView } from "./abstract";
import authStore from "@/stores/auth";

export class LoginView extends AbstractView {
  public title = "Вход";

  async loginSubmit(e: SubmitEvent) {
    e.preventDefault();

    const login = (document.getElementById("login_enter") as HTMLInputElement)
      .value;
    const password = (
      document.getElementById("password_enter") as HTMLInputElement
    ).value;

    const loginData = await loginUser(login, password);
    if (loginData.authenticated) {
      router.redirect("/");
    }
  }

  cancelLogin(e: MouseEvent) {
    e.preventDefault();

    if (authStore.token !== null) {
      router.redirect("/");
    }
  }

  async getHtml(): Promise<TemplateResult> {
    return new ModalLayout().getHtml(
      html`
        <form @submit=${this.loginSubmit}>
          <h2>Авторизация</h2>
          <div>
            <label for="login_enter">Логин</label>
            <input type="text" name="login" id="login_enter" />
          </div>
          <div>
            <label for="password_enter">Пароль</label>
            <input type="password" name="password" id="password_enter" />
          </div>
          <div>
            <button type="submit" class="approve_button">Отправить</button>
            <button @click=${this.cancelLogin} class="cancel_button">
              Отмена
            </button>
          </div>
        </form>
      `
    );
  }
}
