import { html, TemplateResult } from "lit-html";
import { DefaultLayout } from "@/layouts/default";
import { AbstractView } from "./abstract";

export class ShiftsView extends AbstractView {
  public title = "Смены";

  async getHtml(): Promise<TemplateResult> {
    return new DefaultLayout().getHtml(
      html`<section class="shift">
        <button class="approve_button">Добавить смену</button>
        <article>
          <h2>Смена №5</h2>
          <p>Начало смены в 2021-09-19 08:00</p>
          <p>Конец смены в 2021-09-19 18:00</p>
          <p class="working">Статус: Открыта</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Смена №4</h2>
          <p>Начало смены в 2021-09-18 08:00</p>
          <p>Конец смены в 2021-09-18 18:00</p>
          <p class="fired">Статус: Закрыта</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Смена №3</h2>
          <p>Начало смены в 2021-09-17 08:00</p>
          <p>Конец смены в 2021-09-17 18:00</p>
          <p class="fired">Статус: Закрыта</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Смена №2</h2>
          <p>Начало смены в 2021-09-16 08:00</p>
          <p>Конец смены в 2021-09-16 18:00</p>
          <p class="fired">Статус: Закрыта</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Смена №1</h2>
          <p>Начало смены в 2021-09-15 08:00</p>
          <p>Конец смены в 2021-09-15 18:00</p>
          <p class="fired">Статус: Закрыта</p>
          <button class="approve_button">Управление</button>
        </article>
      </section>`
    );
  }
}
