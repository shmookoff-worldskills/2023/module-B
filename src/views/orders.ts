import { html, TemplateResult } from "lit-html";
import { DefaultLayout } from "@/layouts/default";
import { AbstractView } from "./abstract";

export class OrdersView extends AbstractView {
  public title = "Заказы";

  async getHtml(): Promise<TemplateResult> {
    return new DefaultLayout().getHtml(
      html` <section class="orders">
        <button class="approve_button">Принять заказ</button>
        <article>
          <h2>Столик №1</h2>
          <p>Официант: John</p>
          <p class="fired">Статус: Отмена</p>
          <p>Цена: 9238</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Столик №2</h2>
          <p>Официант: John</p>
          <p class="working">Статус: Принят</p>
          <p>Цена: 238</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Столик №3</h2>
          <p>Официант: John</p>
          <p class="working">Статус: Готов</p>
          <p>Цена: 1436</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Столик №4</h2>
          <p>Официант: John</p>
          <p class="working">Статус: Оплачен</p>
          <p>Цена: 2316</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Столик №5</h2>
          <p>Официант: John</p>
          <p class="working">Статус: Оплачен</p>
          <p>Цена: 345345</p>
          <button class="approve_button">Управление</button>
        </article>
        <article>
          <h2>Столик №6</h2>
          <p>Официант: Mike</p>
          <p class="working">Статус: Оплачен</p>
          <p>Цена: 1212</p>
          <button class="approve_button">Управление</button>
        </article>
      </section>`
    );
  }
}
