import { ApiError, ApiResponseData, fetchApi } from "./fetchApi";
import authStore from "../stores/auth";

type LoginResponse = ApiResponseData<{ user_token: string }>;
type LogoutResponse = ApiResponseData<{ message: "logout" }>;

export const loginUser = async (
  login: string,
  password: string
): Promise<{ authenticated: boolean }> => {
  try {
    const res = await fetchApi<LoginResponse>("login", {
      method: "POST",
      body: { login, password },
      clearAuthOnUnauthorized: true,
    });
    authStore.token = res.data.user_token;
    return { authenticated: true };
  } catch (err) {
    if (err instanceof ApiError && err.data.error.code === 401) {
      return { authenticated: false };
    } else throw err;
  }
};

export const logoutUser = async (): Promise<{ authenticated: false }> => {
  try {
    await fetchApi<LogoutResponse>("logout");
  } catch {
  } finally {
    authStore.token = null;
  }
  return { authenticated: false };
};
