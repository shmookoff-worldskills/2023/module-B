import { ApiResponseData, fetchApi } from "./fetchApi";

export enum UserStatus {
  Free = 0,
  Working = 1,
}
export enum UserGroup {
  Administrator = 0,
  Server = 1,
  Cook = 2,
}

interface UserBase {
  name: string;
  login: string;
}

interface UserRead extends UserBase {
  id: number;
  status: UserStatus;
  group: UserGroup;
}

type GetUsersResponse = ApiResponseData<UserRead[]>;

export const getUsers = async (): Promise<UserRead[]> => {
  const res = await fetchApi<GetUsersResponse>("user");
  return res.data;
};

// interface UserCreate extends UserBase {
//   password: string;
//   role_id: UserGroup;
//   photo_file: File;
// }

interface UserCreateResponse {
  id: number;
  status: "created";
}
type PostUserResponse = ApiResponseData<UserCreateResponse>;

export const postUser = async (
  form: HTMLFormElement
): Promise<UserCreateResponse> => {
  const res = await fetchApi<PostUserResponse>("user", {
    body: new FormData(form),
    method: "POST",
  });
  return res.data;
};
