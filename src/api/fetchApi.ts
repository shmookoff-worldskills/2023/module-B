import authStore from "../stores/auth";

interface ApiErrorData {
  error: {
    code: number;
    message: string;
    errors?: { [key: string]: string[] };
  };
}

export interface ApiResponseData<T extends Object> {
  data: T;
}

export class ApiError extends Error {
  data: ApiErrorData;

  constructor(data: ApiErrorData, options?: ErrorOptions | undefined) {
    super(data.error.message, options);
    this.data = data;
  }
}

interface ApiRequestConfig extends Omit<RequestInit, "body"> {
  body?: object;
  clearAuthOnUnauthorized?: boolean;
}

export const fetchApi = async <T extends Object>(
  endpoint: RequestInfo | URL,
  init?: ApiRequestConfig | undefined
): Promise<T> => {
  const clearAuthOnUnauthorized =
    init?.clearAuthOnUnauthorized === undefined
      ? true
      : init.clearAuthOnUnauthorized;

  const config: RequestInit = {
    headers: constructHeaders(init?.method, init?.body),
    ...init,
    body: constructBody(init?.body),
  };

  const response = await window.fetch(
    `${import.meta.env.VITE_API_HOST}/${endpoint}`,
    config
  );

  const data = await response.json();

  if (!response.ok) {
    if (clearAuthOnUnauthorized && response.status === 401) {
      authStore.token = null;
    }
    throw new ApiError(data);
  }

  return data;
};

const constructHeaders = (method?: string, body?: object) => {
  const headers = new Headers({ Accept: "application/json" });

  if (
    method &&
    ["POST", "PUT"].includes(method) &&
    !(body instanceof FormData)
  ) {
    headers.append("Content-Type", "application/json");
  }

  if (authStore.token) {
    headers.append("Authorization", `Bearer ${authStore.token}`);
  }

  return headers;
};

const constructBody = (body?: object): FormData | string | undefined => {
  if (body instanceof FormData) return body;
  if (body) return JSON.stringify(body);
  return undefined;
};
