import { TemplateResult } from "lit-html";

export abstract class AbstractComponent {
  abstract render(): TemplateResult;
}
