import { UserGroup, UserStatus } from "@/api/user";
import { html, TemplateResult } from "lit-html";
import { AbstractComponent } from "./abstract";

const statusToClass = {
  [UserStatus.Free]: "fired",
  [UserStatus.Working]: "working",
};

interface EmployeeData {
  name: string;
  status: UserStatus;
  group: UserGroup;
}

export class EmployeeComponent
  extends AbstractComponent
  implements EmployeeData
{
  name: string;
  status: UserStatus;
  group: UserGroup;

  constructor({ name, status, group }: EmployeeData) {
    super();
    this.name = name;
    this.status = status;
    this.group = group;
  }

  render(): TemplateResult {
    return html`<article>
      <span>${this.name}</span>
      <span class="${statusToClass[this.status]}"
        >${UserStatus[this.status]}</span
      >
      <span>${UserGroup[this.group]}</span>
      <button>Подробнее</button>
    </article>`;
  }
}
