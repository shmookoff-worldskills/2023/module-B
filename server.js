import express from "express";
import path, { dirname } from "path";
import { fileURLToPath } from "url";
const app = express();

const __dirname = dirname(fileURLToPath(import.meta.url));

app.use("/assets", express.static(path.resolve(__dirname, "dist", "assets")));

app.get("/*", (_, res) => {
  res.sendFile(path.resolve("dist", "index.html"));
});

const PORT = 5173;

app.listen(PORT, () => {
  console.log(`http://localhost:${PORT}`);
});
