import { defineConfig } from "vite";
import url from "url";

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: [
      {
        find: "@", replacement: url.fileURLToPath(new url.URL("./src", import.meta.url)),
      },
    ],
  },
});
